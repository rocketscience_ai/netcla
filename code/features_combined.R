#-------------------------------------------------------------------------------
#
# NetCla: The ECML-PKDD Network Classification Challenge
# Features Creator: Combined Features
#
# Code style follows 'Google R Style Guide' with tweaks from Hadley Wickham: 
# https://google.github.io/styleguide/Rguide.xml
# http://adv-r.had.co.nz/Style.html
#
#-------------------------------------------------------------------------------

rm(list = ls(all.names = TRUE))
gc(reset = TRUE)
set.seed(12345)

library(magrittr)
library(plyr)
library(dplyr)
library(ggplot2)
library(stringi)


# (I) Loads & combines data-----------------------------------------------------

suffix <- c("clean", "transform")

header <- c()
train <- NULL
valid <- NULL
test <- NULL

for (s in suffix) {
  
  # Header
  .header <- readRDS(file = sprintf("features/%s_header.RData", s))
  header <- c(header, .header[!.header %in% header])
  
  # Train
  .train <- readRDS(file = sprintf("features/%s_train.RData", s))
  train %<>% bind_cols(.train[, !colnames(.train) %in% colnames(train)])
  
  # Valid
  .valid <- readRDS(file = sprintf("features/%s_valid.RData", s))
  valid %<>% bind_cols(.valid[, !colnames(.valid) %in% colnames(valid)])
  
  # Test
  .test <- readRDS(file = sprintf("features/%s_test.RData", s))
  test %<>% bind_cols(.test[, !colnames(.test) %in% colnames(test)])
}


# (II) Saves features-----------------------------------------------------------

suffix <- "combined"

saveRDS(header, file = sprintf("features/%s_header.RData", suffix))
saveRDS(train, file = sprintf("features/%s_train.RData", suffix))
saveRDS(valid, file = sprintf("features/%s_valid.RData", suffix))
saveRDS(test, file = sprintf("features/%s_test.RData", suffix))


# (III) Removes bad variables---------------------------------------------------

bad_variables <- readRDS("features/bad_variables.RData")

header <- header[!header %in% bad_variables]
train <- train[, c(header, "is_app", "app")]
valid <- valid[, c(header, "is_app", "app")]
test <- test[, header]

suffix <- "combined_pruned"

saveRDS(header, file = sprintf("features/%s_header.RData", suffix))
saveRDS(train, file = sprintf("features/%s_train.RData", suffix))
saveRDS(valid, file = sprintf("features/%s_valid.RData", suffix))
saveRDS(test, file = sprintf("features/%s_test.RData", suffix))


