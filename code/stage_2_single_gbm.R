#-------------------------------------------------------------------------------
#
# NetCla: The ECML-PKDD Network Classification Challenge
# Code for Stage #2: Single GBM model
#
# Code style follows 'Google R Style Guide' with tweaks from Hadley Wickham: 
# https://google.github.io/styleguide/Rguide.xml
# http://adv-r.had.co.nz/Style.html
#
#-------------------------------------------------------------------------------

#' Returns predictions for Stage #1
#' 
stage_2 <- function(out_1) {
  
  # Trains model
  message("Training...")
  model_gbm <- h2o.gbm(
    x = header,
    y = "app",
    training_frame = dx_train_2,
    model_id = "stage_2_single_gbm",
    validation_frame = dx_valid_2,
    ntrees = 101,
    nbins = 20,
    nfolds = 5,
    balance_classes = TRUE,
    max_after_balance_size = 10,
    stopping_metric = "mean_per_class_error"
  )
  
  # Predicts values
  message("Prediction...")
  pred <- h2o.predict(model_gbm, newdata = dx_valid)
  
  # Creates output
  message("Preparing results...")
  out_2 <- as.data.frame(pred) %>% 
    select(predict) %>%
    mutate(predict = as.character(predict) %>% as.numeric()) %>%
    tbl_df()
  
  # Corrects for 1st step
  indx <- which(out_1$predictions == 0)
  out_2$predict[indx] <- 0
  
  # Returns
  list(predictions = out_2, fit = model_gbm)
}

