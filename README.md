# README

NetCla: The ECML-PKDD Network Classification Challenge

http://www.ecmlpkdd2016.org/submission.html#CallDiscovery
http://www.neteye-blog.com/netcla-the-ecml-pkdd-network-classification-challenge/

===

# NetCla: The ECML-PKDD Network Classification Challenge

## Data

The `NetCla/data/` folder contains the official dataset for the competition.
Both training and validation sets are separated into two different files:
`train.csv`, for example, contains the list of data points of the training set whereas
`train_target.csv contains the labels for these examples.

Each line in the file represents a data point and its features are represented in tab-separated format. The two splits (train and validation) contain 761,179 data points each.


## Submission

The submission format is the same as the gold standard file format. 
Each line is a data point and contains the application with its ID.

Participants must align the lines of the prediction file with those of the data point file. 

An example of submission file can be found in the "baseline" subfolder.
`valid_dt.csv` is the output of our decision tree classifier in the submission format.


## Evaluation

The participant's performance will be evaluated with the following metrics:

- Micro-Recall
- Macro-Recall
- Micro-Precision
- Macro-Precision
- Micro-F1
- Macro-F1

The final ranking will be derived based on *** Macro-F1 *** evaluated on the test set.

All the measures do not include the true positive from the "Unknown Application" class (ID number 0).


The challenge scorer `eval.py` can be found into the download subfolder.
To run it, one needs to provide the target file and the submission file.
For instance, the following command scores the baseline output we provided:

`python eval.py ../data/valid_target.csv ../data/valid_dt.csv`


## Test Data

Dear NetCla participant,

thank you for your interest in the challenge. Today we are releasing the test set. 

Below some important information follows:

- download the test set using the following link: http://goo.gl/GyvgDQ.

- From now, you will have about three days to submit your run: the submission site closes on Saturday, September 10 at 23:59 CET.

- To submit a run, please fill the form following this link: http://goo.gl/QAEWTT.

- Each team has to provide a single "primary run" on the test set for the official competition, which will be the only one used to officially rank systems.
Additionally, they can provide up to 2 contrastive runs on the validation and two on the test sets (for a total of 5 runs).  These are not mandatory and serve only the purpose of showing the potential of the different participating systems.

- Each run must be in a separate file in the format accepted by the evaluation script (provided along with the training data zip file). Please take a look at the example run on the validation set to verify the submission format.

- Put all files in a single zip archive. 

- The individual files should be named as follows: 
	teamName-runNumber-dataset-type.csv, 
where the
	-- teamName is your team name, 
	-- runNumber is the number of the run, 
	-- dataset is either "test" or “validation",
	-- type is either "primary" or contrastive" (you have to mark exactly one run as "primary").

-  You will have the possibility to revise your submission until the deadline, by filling the same form (the email and names cannot be changed).

We will only consider the last submission from each team.


Good luck with your submission,
the NetCla team.

